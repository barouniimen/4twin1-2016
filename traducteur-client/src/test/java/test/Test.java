package test;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import traducteur.business.ITraducteurRemote;

public class Test {

	public static void main(String[] args) {
		
		try {
			Context context = new  InitialContext();
			ITraducteurRemote remote = (ITraducteurRemote) context.lookup("/traducteur/Traducteur!traducteur.business.ITraducteurRemote");
			System.out.println(remote.traduire("bonjour"));
			
			
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
