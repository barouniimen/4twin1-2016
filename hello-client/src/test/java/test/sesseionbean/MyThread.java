package test.sesseionbean;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import hello.sessionbean.IHelloRemote;

public class MyThread extends Thread{
	
	@Override
	public void run() {
		try {
			//1- se connecter au serveur
			Context context = new InitialContext();

			//2- chercher une instance de Hello par JNDI name
			//3- R�cup�rer un proxy sur l'instance
			
			String appName = "hello";
			String sessionBeanName ="Hello";
			String interfaceCanonicalName = "hello.sessionbean.IHelloRemote";
			String jndiName = "/"+appName+"/"+sessionBeanName+"!"+interfaceCanonicalName;
			
			IHelloRemote helloRemote= (IHelloRemote) context.lookup(jndiName);
			//System.out.println(helloRemote.sayHello());
			System.out.println("je suis le thread num "+this.getId()+"j'appel :"+ helloRemote.returnHashcode());
			System.out.println("je suis le thread num "+this.getId()+"j'appel :"+ helloRemote.returnHashcode());
			
			
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
