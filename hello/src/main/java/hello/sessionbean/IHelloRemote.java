package hello.sessionbean;

import javax.ejb.Remote;

@Remote
public interface IHelloRemote {

	public String sayHello();
	public int returnHashcode();
}
