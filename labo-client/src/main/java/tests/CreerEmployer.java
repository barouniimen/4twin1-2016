package tests;

import javax.naming.NamingException;

import tn.esprit.perso.labo.persistence.Admin;
import tn.esprit.perso.labo.persistence.Employe;
import tn.esprit.perso.labo.services.GestionLaboRemote;

public class CreerEmployer {
	public static void main(String[] args) throws NamingException {
		GestionLaboRemote proxy = InvoquerProxy.returnProxy();
		Admin employe = new Admin();
		employe.setCin("2");
		employe.setLogin("admin");
		employe.setPassword("admin");
		proxy.ajouterEmploye(employe);
	}
}
