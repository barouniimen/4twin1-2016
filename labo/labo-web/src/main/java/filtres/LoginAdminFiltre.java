package filtres;

import java.io.IOException;

import javax.faces.context.FacesContext;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tn.esprit.perso.labo.persistence.Admin;
import tn.esprit.perso.labo.presentation.mbeans.AuthentificatinBean;

@WebFilter(urlPatterns = "/admin/*")
public class LoginAdminFiltre implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		AuthentificatinBean authentificatinBean = (AuthentificatinBean) req.getSession().getAttribute("authentificatinBean");
	if(authentificatinBean.getEmploye() instanceof Admin){
		chain.doFilter(request, response);
	}
	else{
		resp.sendRedirect(req.getContextPath()+"/public/login.jsf?faces-redirect=true");
				
	}
	
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

}
