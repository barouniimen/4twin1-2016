package tn.esprit.perso.labo.presentation.mbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

import tn.esprit.perso.labo.persistence.Labo;
import tn.esprit.perso.labo.services.GestionLaboLocal;

@ManagedBean
@SessionScoped
public class ManageLaboBean implements Serializable {
	private Labo labo;
	@EJB
	GestionLaboLocal gestionLaboLocal;
	
	
	@PostConstruct
	public void init(){
		labo = new Labo();
		
		
	}
	public String addLabo(){
		labo = gestionLaboLocal.creerLabo(labo);
		return "manageEmploye?faces-redirect=true";
	}
	
	public Labo getLabo() {
		return labo;
	}
	public void setLabo(Labo labo) {
		this.labo = labo;
	}
}
