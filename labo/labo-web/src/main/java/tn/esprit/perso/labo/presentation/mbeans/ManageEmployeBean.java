package tn.esprit.perso.labo.presentation.mbeans;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import tn.esprit.perso.labo.persistence.Employe;
import tn.esprit.perso.labo.persistence.Labo;
import tn.esprit.perso.labo.services.GestionLaboLocal;

@ManagedBean
@ViewScoped
public class ManageEmployeBean implements Serializable {
	@EJB
	GestionLaboLocal gestionLaboLocal;
	private List<Employe> employes;
	private Boolean renderAddForm;
	private Boolean renderAddBtn;
	private Boolean renderUpdateBtn;
	private Boolean renderList;
	private Boolean renderRemoveBtn;
	@ManagedProperty(value = "#{manageLaboBean}")
	private ManageLaboBean laboBean;

	private Employe employeToAdd;

	@PostConstruct
	public void init() {
		setEmployeToAdd(new Employe());
		employes = gestionLaboLocal.listerEmployesLabo(laboBean.getLabo());

		if (employes.isEmpty()) {
			System.out.println("is empty");
			// msg empty et formulaire d'ajout
			setRenderAddForm(true);
			renderAddBtn = true;
			renderUpdateBtn = false;
			renderRemoveBtn = false;
			renderList = false;
			FacesContext.getCurrentInstance().addMessage("idform", new FacesMessage("la liste est vide!"));
		} else {
			// msg add et liste et bouton d'ajout optionnel
			System.out.println("is not empty");

			renderAddBtn = false;
			renderUpdateBtn = false;
			renderRemoveBtn = false;
			setRenderAddForm(false);

			renderList = true;
		}
	}

	public String update() {
		gestionLaboLocal.modifierEmploye(employeToAdd);
		//renderUpdateBtn = false;
		return "/admin/manageEmploye?faces-redirect=true";
	}

	public String removeEmploye(Employe employe) {
		gestionLaboLocal.demissionnerEmploye(employe);
	//	renderRemoveBtn = false;
		return "/admin/manageEmploye?faces-redirect=true";
	}

	public String addEmploye() {
		employeToAdd.setLabo(laboBean.getLabo());
		gestionLaboLocal.ajouterEmploye(employeToAdd);
		//renderAddBtn = false;
		return "/admin/manageEmploye?faces-redirect=true";
	}

	public ManageLaboBean getLaboBean() {
		return laboBean;
	}

	public void setLaboBean(ManageLaboBean laboBean) {
		this.laboBean = laboBean;
	}

	public List<Employe> getEmployes() {
		return employes;
	}

	public void setEmployes(List<Employe> employes) {
		this.employes = employes;
	}

	public Boolean getRenderAddForm() {
		return renderAddForm;
	}

	public void setRenderAddForm(Boolean renderAddForm) {
		this.renderAddForm = renderAddForm;
	}

	public Boolean getRenderList() {
		return renderList;
	}

	public void setRenderList(Boolean renderList) {
		this.renderList = renderList;
	}

	public Employe getEmployeToAdd() {
		return employeToAdd;
	}

	public void setEmployeToAdd(Employe employeToAdd) {
		this.employeToAdd = employeToAdd;
	}

	public Boolean getRenderAddBtn() {
		return renderAddBtn;
	}

	public void setRenderAddBtn(Boolean renderAddBtn) {
		this.renderAddBtn = renderAddBtn;
	}

	public Boolean getRenderUpdateBtn() {
		return renderUpdateBtn;
	}

	public void setRenderUpdateBtn(Boolean renderUpdateBtn) {
		this.renderUpdateBtn = renderUpdateBtn;
	}

	public Boolean getRenderRemoveBtn() {
		return renderRemoveBtn;
	}

	public void setRenderRemoveBtn(Boolean renderRemoveBtn) {
		this.renderRemoveBtn = renderRemoveBtn;
	}

}
