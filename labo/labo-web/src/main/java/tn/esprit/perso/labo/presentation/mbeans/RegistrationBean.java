package tn.esprit.perso.labo.presentation.mbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import tn.esprit.perso.labo.persistence.Admin;
import tn.esprit.perso.labo.persistence.Employe;
import tn.esprit.perso.labo.persistence.Technicien;
import tn.esprit.perso.labo.services.GestionLaboLocal;

@ManagedBean
@RequestScoped
public class RegistrationBean implements Serializable {

	private List<String> typesEmplyes;
	private String selectedEmploye;
	private Employe employeToCreate;

	@EJB
	GestionLaboLocal gestionLabo;

	@PostConstruct
	public void init() {
		employeToCreate = new Employe();
		typesEmplyes = new ArrayList<String>();
		typesEmplyes.add("employe");
		typesEmplyes.add("administrateur");
		typesEmplyes.add("technicien");
	}

	public String register() {
		System.out.println("emp "+employeToCreate);
		if (employeToCreate.getLogin().isEmpty() && employeToCreate.getPassword().isEmpty()) {
			System.out.println("is nulll");
			FacesContext.getCurrentInstance().addMessage("idFormReg", new FacesMessage("login et pwd obligatoires"));
			return null;
		} else {
			if (selectedEmploye.equals("employe")) {
				gestionLabo.ajouterEmploye(employeToCreate);
				return "/public/login?faces-redirect=true";

			} else if (selectedEmploye.equals("administrateur")) {
				Admin admin = new Admin();
				admin.setCin(employeToCreate.getCin());
				admin.setLogin(employeToCreate.getLogin());
				admin.setPassword(employeToCreate.getPassword());
				admin.setNom(employeToCreate.getNom());
				admin.setPrenom(employeToCreate.getPrenom());
				admin.setMail(employeToCreate.getMail());
			//	admin.setDateNaissance(employeToCreate.getDateNaissance());
				gestionLabo.ajouterEmploye(admin);
				return "/public/login?faces-redirect=true";
			} else {
				Technicien tech = new Technicien();
				tech.setCin(employeToCreate.getCin());
				tech.setLogin(employeToCreate.getLogin());
				tech.setPassword(employeToCreate.getPassword());
				tech.setNom(employeToCreate.getNom());
				tech.setPrenom(employeToCreate.getPrenom());
				tech.setMail(employeToCreate.getMail());
				//tech.setDateNaissance(employeToCreate.getDateNaissance());
				gestionLabo.ajouterEmploye(tech);
				return "/public/login?faces-redirect=true";
			}
		}
	}

	public List<String> getTypesEmplyes() {
		return typesEmplyes;
	}

	public void setTypesEmplyes(List<String> typesEmplyes) {
		this.typesEmplyes = typesEmplyes;
	}

	public String getSelectedEmploye() {
		return selectedEmploye;
	}

	public void setSelectedEmploye(String selectedEmploye) {
		this.selectedEmploye = selectedEmploye;
	}

	public Employe getEmployeToCreate() {
		return employeToCreate;
	}

	public void setEmployeToCreate(Employe employeToCreate) {
		this.employeToCreate = employeToCreate;
	}
}
