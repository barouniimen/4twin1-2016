package tn.esprit.perso.labo.presentation.mbeans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import tn.esprit.perso.labo.persistence.Admin;
import tn.esprit.perso.labo.persistence.Chercheur;
import tn.esprit.perso.labo.persistence.Employe;
import tn.esprit.perso.labo.services.GestionLaboLocal;

@ManagedBean
@SessionScoped
public class AuthentificatinBean implements Serializable {
	@EJB
	GestionLaboLocal gestionLaboLocal;
	private Employe employe;

	@PostConstruct
	public void init() {
		employe = new Employe();
		
	}
	
	public String logout(){
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "/public/login?faces-redirect=true";
	}

	public String login() {
		System.out.println("............"+employe.getLogin());
		employe = gestionLaboLocal.authentifier(employe.getLogin(), employe.getPassword());
		if (employe instanceof Admin) {
			return "/admin/acceuil?faces-redirect=true";
		} else if (employe instanceof Chercheur) {
			return "/chercheur/acceuil?faces-redirect=true";
		} else {
			return "/public/login?faces-redirect=true";
		}
	}

	public Employe getEmploye() {
		return employe;
	}

	public void setEmploye(Employe employe) {
		this.employe = employe;
	}

}
